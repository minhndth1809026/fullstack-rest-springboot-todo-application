package com.example.fullstackrestspringboottodoapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullstackRestSpringbootTodoApplication {

    public static void main(String[] args) {
        SpringApplication.run(FullstackRestSpringbootTodoApplication.class, args);
    }

}
