package com.example.fullstackrestspringboottodoapplication.api;

import com.example.fullstackrestspringboottodoapplication.entity.Todo;
import com.example.fullstackrestspringboottodoapplication.service.TodoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/todos")
@RequiredArgsConstructor
public class TodoAPI {
    private final TodoService todoService;

    @GetMapping
    public ResponseEntity<List<Todo>> findAll() {
        return ResponseEntity.ok(todoService.findAll());
    }

    @PostMapping
    public ResponseEntity saveAll(@Valid @RequestBody List<Todo> todos) {
        return ResponseEntity.ok(todoService.saveAll(todos));
    }
}
